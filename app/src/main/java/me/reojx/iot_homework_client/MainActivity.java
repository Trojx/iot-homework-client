package me.reojx.iot_homework_client;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.PropertyKeyConst;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {
    public static final String TAG="MainActivity";
    private TextView tvTemp;
    private TextView tvHumidity;
    private TextView tvCh2o;
    private TextView tvTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTemp = findViewById(R.id.tv_stat_temp);
        tvHumidity = findViewById(R.id.tv_stat_humidity);
        tvCh2o = findViewById(R.id.tv_stat_ch2o);
        tvTime = findViewById(R.id.tv_time);

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.CHINA);
        tvTime.setText("更新时间:"+simpleDateFormat.format(new Date()));

        Thread subThread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    startSubscribe();
                }catch (Exception e){
                    Log.e(TAG,e.getMessage());
                }
            }
        });
        subThread.start();
    }

    private void startSubscribe(){
        Properties properties = new Properties();
        // 您在控制台创建的 Consumer ID
        properties.put(PropertyKeyConst.ConsumerId, "CID_Android1");
        // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.AccessKey, "LTAIWkOzkWDZcjqS");
        // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, "zL49j65Ho5oHTOoydltT82bEJqHHCb");
        // 设置 TCP 接入域名，进入 MQ 控制台的消费者管理页面，在左侧操作栏单击获取接入点获取
        // 此处以公共云生产环境为例
        properties.put(PropertyKeyConst.ONSAddr,
                "http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet");
        Consumer consumer = ONSFactory.createConsumer(properties);
        consumer.subscribe("AirStat", "*", new MessageListener() { //订阅多个 Tag
            public Action consume(Message message, ConsumeContext context) {
                Log.d(TAG,"Receive: " + message);
                final JSONObject stat=JSONObject.parseObject(new String(message.getBody()));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvTemp.setText(stat.getString("temperature"));
                        tvHumidity.setText(stat.getString("humidity"));
                        tvCh2o.setText(stat.getString("ch2o"));
                    }
                });
                return Action.CommitMessage;
            }
        });
        consumer.start();
    }
}